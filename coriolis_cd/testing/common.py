# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import functools
import json
import time
import traceback

from barbicanclient import client as barbican_client
from coriolisclient import client
import keystoneauth1
from keystoneauth1.identity import v3
from keystoneauth1 import session as ksession


def get_exception_details():
    return traceback.format_exc()


def retry_on_error(max_attempts=30, sleep_seconds=5, terminal_exceptions=[]):
    def _retry_on_error(func):
        @functools.wraps(func)
        def _exec_retry(*args, **kwargs):
            i = 0
            while True:
                try:
                    return func(*args, **kwargs)
                except KeyboardInterrupt as ex:
                    print("Got a KeyboardInterrupt, skip retrying")
                    print(ex)
                    raise
                except Exception as ex:
                    if any([isinstance(ex, tex)
                            for tex in terminal_exceptions]):
                        raise
                    i += 1
                    if i < max_attempts:
                        print("Exception occurred, retrying (%d/%d):\n%s" % (
                            i, max_attempts, get_exception_details()))
                        time.sleep(sleep_seconds)
                    else:
                        raise
        return _exec_retry
    return _retry_on_error


def get_single_result(lis):
    """Indexes the head of a single element list.

    :raises KeyError: if the list is empty or its length is greater than 1.
    """
    if len(lis) == 0:
        raise KeyError("Result list is empty.")
    elif len(lis) > 1:
        raise KeyError("More than one result in list: '%s'" % lis)

    return lis[0]


class CoriolisSession(object):
    def __init__(self, auth_url, username, password, project_name,
                 user_domain_name="Default", project_domain_name="Default",
                 verify=True):
        CONN_INFO = {
            "auth_url": auth_url,
            "username": username,
            "password": password,
            "project_name": project_name,
            "user_domain_name": user_domain_name,
            "project_domain_name": project_domain_name
        }
        self._session = ksession.Session(auth=v3.Password(**CONN_INFO),
                                         verify=verify)

    def get_client(self):
        return client.Client(session=self._session)

    @retry_on_error()
    def get_barbican_secret_payload(self, secret_ref):
        barbican = barbican_client.Client(session=self._session)
        sec = barbican.secrets.get(secret_ref)
        # NOTE: accessing `payload` leads to another API call being made:
        payload = getattr(sec, "payload")
        return payload

    def get_secret_connection_info(self, connection_info):
        secret_ref = connection_info.get("secret_ref")
        if secret_ref:
            print("Retrieving connection info from secret: %s", secret_ref)
            try:
                connection_info = json.loads(
                    self.get_barbican_secret_payload(secret_ref))
            except keystoneauth1.exceptions.http.Unauthorized:
                print("Error occurred while fetching secret with trust ID.")
                raise
            except Exception as e:
                print("Exception while retrieving the secret:", e)
                raise
        return connection_info
