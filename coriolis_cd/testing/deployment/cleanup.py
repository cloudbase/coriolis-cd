# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import base64
import copy
import os
import ovirtsdk4
import pylxd
import tempfile
import time
from typing import Any
from typing import Dict

from abc import abstractmethod
from oci.exceptions import ServiceError

from cinderclient import client as cinder_client
from keystoneauth1.identity import v3
from keystoneauth1 import session as ksession
from neutronclient.neutron import client as neutron_client
from novaclient import client as nova_client

from coriolis_provider_azure import utils as azutils
from coriolis_provider_lxd import common as lxdutils
from coriolis_provider_oci.common import OCIClient
from coriolis_provider_oracle_vm import util as ovmutils
from coriolis_provider_ovirt import utils as ovirtutils
from coriolis_provider_proxmox import common as proxmoxutils

from coriolis_cd.providers import vmware
from coriolis_cd.testing.common import get_single_result
from coriolis_cd.testing.common import retry_on_error
LxdEnvInfo = Dict[str, Any]


def get_cleanup_provider(provider_type, connection_info,
                         destination_env, instance_result):
    if provider_type == "openstack":
        return OpenStackDeploymentCleanup(
            instance_result, connection_info, destination_env)
    elif provider_type == "azure":
        return AzureDeploymentCleanup(
            instance_result, connection_info, destination_env)
    elif provider_type == "oracle_vm":
        return OracleVMDeploymentCleanup(
            instance_result, connection_info, destination_env)
    elif provider_type == "oci":
        return OCIDeploymentCleanup(
            instance_result, connection_info, destination_env)
    elif provider_type == "vmware_vsphere":
        return VMWareDeploymentCleanup(
            instance_result, connection_info, destination_env)
    elif provider_type in ['olvm', 'rhev']:
        return OvirtDeploymentCleanup(
            instance_result, connection_info, destination_env)
    elif provider_type == "lxd":
        return LxdDeploymentCleanup(
            instance_result, connection_info, destination_env)
    elif provider_type == "proxmox":
        return ProxmoxDeploymentCleanup(
            instance_result, connection_info, destination_env)
    else:
        raise Exception("Cannot find cleanup class for: %s" % (
            provider_type))


class DeploymentCleanup(object):

    def __init__(self, instance_result, connection_info, destination_env):
        self._instance_result = instance_result
        self._destination_env = destination_env
        self._conn_info = connection_info

    @abstractmethod
    def cleanup(self):
        pass


class OpenStackDeploymentCleanup(DeploymentCleanup):

    def __init__(self, instance_result, connection_info, destination_env):
        super(OpenStackDeploymentCleanup, self).__init__(
            instance_result, connection_info, destination_env)
        conn_info = {
            "auth_url": connection_info["auth_url"],
            "username": connection_info["username"],
            "password": connection_info["password"],
            "project_name": connection_info["project_name"],
            "user_domain_name": connection_info["user_domain_name"],
            "project_domain_name": connection_info["project_domain_name"]
        }
        verify = not connection_info.get('allow_untrusted', False)
        session = ksession.Session(
            auth=v3.Password(**conn_info), verify=verify)
        self._nova = nova_client.Client(2, session=session)
        self._cinder = cinder_client.Client(3, session=session)
        self._neutron = neutron_client.Client('2.0', session=session)

    @retry_on_error()
    def _wait_for_instance_deletion(self,
                                    instance_id,
                                    max_retries=150,
                                    retry_period=2):
        instances = self._nova.servers.findall(id=instance_id)
        i = 0
        while i < max_retries and instances:
            i = i + 1
            instance = get_single_result(instances)
            if instance.status == 'error':
                raise Exception(
                    "Instance \"%s\" has reached invalid state \"%s\" while "
                    "deleting." % (instance_id, instance.status))

            print("Instance %s status: %s. Waiting %s seconds for its "
                  "deletion." % (instance_id, instance.status, retry_period))
            time.sleep(retry_period)
            instances = self._nova.servers.findall(id=instance_id)

        if instances:
            instance = get_single_result(instances)
            raise Exception(
                "Max attempts of %s reached while waiting for VM \"%s\" "
                "deletion. Last known status: \"%s\"" % (max_retries,
                                                         instance.status,
                                                         instance_id))

    @retry_on_error()
    def _delete_instance(self):
        instances = self._nova.servers.findall(
            name=self._instance_result["name"])
        if not instances:
            print("Cannot find instance '%s'. Considering it already "
                  "deleted." % self._instance_result["name"])
            return

        instance = get_single_result(instances)

        self._nova.servers.delete(instance)
        self._wait_for_instance_deletion(instance.id)

    @retry_on_error()
    def _delete_volumes(self):
        for disk in self._instance_result["devices"]["disks"]:
            volumes = self._cinder.volumes.findall(id=disk["id"])
            for volume in volumes:
                volume.delete()

    @retry_on_error()
    def _delete_floating_ip(self):
        public_address = self._instance_result["public_ip_address"]
        floating_ips = [
            ip for ip in self._neutron.list_floatingips()["floatingips"]
            if ip["floating_ip_address"] == public_address
        ]
        if not floating_ips:
            print("Cannot find floating ip '%s'. Consindering it already "
                  "deleted." % public_address)
            return

        floating_ip = get_single_result(floating_ips)

        self._neutron.delete_floatingip(floating_ip["id"])

    def _delete_network_ports(self):
        for nic in self._instance_result["devices"]["nics"]:
            ports = self._neutron.list_ports(id=nic["id"])["ports"]
            for port in ports:
                self._neutron.delete_port(port["id"])

    def cleanup(self):
        self._delete_instance()
        self._delete_volumes()
        self._delete_floating_ip()
        self._delete_network_ports()


class AzureDeploymentCleanup(DeploymentCleanup):

    def __init__(self, instance_result, connection_info, destination_env):
        super(AzureDeploymentCleanup, self).__init__(
            instance_result, connection_info, destination_env)
        self._cli = azutils.AzureClient(connection_info)

    def _delete_instance(self):
        print("Shutting down instance '%s'" % self._instance_result["name"])
        retry_on_error()(
            self._cli.compute_client.virtual_machines.power_off)(
                self._destination_env["resource_group"],
                self._instance_result["name"],
                skip_shutdown=True).result()
        print("Deleting instance '%s'" % self._instance_result["name"])
        retry_on_error()(
            self._cli.compute_client.virtual_machines.delete)(
                self._destination_env["resource_group"],
                self._instance_result["name"]).result()

    def _delete_volumes(self):
        for disk in self._instance_result["devices"]["disks"]:
            print("Deleting instance disk '%s'" % disk["name"])
            retry_on_error()(
                self._cli.compute_client.disks.delete)(
                    self._destination_env["resource_group"],
                    disk["name"]).result()

    def _delete_nics(self):
        for nic in self._instance_result["devices"]["nics"]:
            azure_nic = retry_on_error()(
                self._cli.network_client.network_interfaces.get)(
                    self._destination_env["resource_group"],
                    nic["name"])
            print("Deleting instance NIC '%s'" % nic["name"])
            retry_on_error()(
                self._cli.network_client.network_interfaces.delete)(
                    self._destination_env["resource_group"],
                    nic["name"]).result()
            for ipconf in azure_nic.ip_configurations:
                pip_id = ipconf.public_ip_address.id
                if not pip_id:
                    continue
                pip_parts = azutils.parse_resource_id(pip_id)
                print("Deleting public IP '%s'" % pip_parts['name'])
                retry_on_error()(
                    self._cli.network_client.public_ip_addresses.delete)(
                        pip_parts['resource_group'],
                        pip_parts['name']).result()

    def cleanup(self):
        self._delete_instance()
        self._delete_volumes()
        self._delete_nics()  # It will delete refereced public IPs as well


class OracleVMDeploymentCleanup(DeploymentCleanup, ovmutils.OVMMixin):

    def __init__(self, instance_result, connection_info, destination_env):
        super(OracleVMDeploymentCleanup, self).__init__(
            instance_result, connection_info, destination_env)
        self._client = ovmutils.get_ovm_client(connection_info)

    def _delete_instance(self):
        vm = self._get_vm_by_name_or_id(
            self._client, self._instance_result["id"])
        self._check_job(self._client, lambda: self._client.vms.kill(vm["id"]))
        self._client.vms.delete(vm["id"])

    def _delete_volumes(self):
        ids = []
        for disk in self._instance_result["devices"]["disks"]:
            ids.append(disk["id"])
        self._delete_virtual_disks(self._client, ids)

    def cleanup(self):
        self._delete_instance()
        self._delete_volumes()


class OCIDeploymentCleanup(DeploymentCleanup):

    def __init__(self, instance_result, connection_info, destination_env):
        super(OCIDeploymentCleanup, self).__init__(
            instance_result, connection_info, destination_env)
        self._cli = OCIClient.from_connection_info(connection_info)

    def _delete_instance(self):
        try:
            instance = self._cli.compute.get_instance(
                self._instance_result["id"])
        except ServiceError as err:
            if getattr(err, "code") == 404:
                return
        if instance.data.lifecycle_state == "TERMINATED":
            return
        self._cli.compute.terminate_instance(
            self._instance_result["id"])
        self._cli.wait_for_machine_status(
            self._instance_result["id"], "TERMINATED", fail_on_404=False)

    def _is_boot_volume(self, volume_id):
        instance = retry_on_error()(self._cli.compute.get_instance)(
            self._instance_result["id"]).data
        return (instance.source_details.boot_volume_id == volume_id)

    def _delete_volumes(self):
        for volume in self._instance_result["devices"]["disks"]:
            if self._is_boot_volume(volume["id"]):
                list_fn = self._cli.block_storage.list_boot_volume_backups
                snap_delete = self._cli.block_storage.delete_boot_volume_backup
                vol_delete = self._cli.block_storage.delete_boot_volume
                list_args = {
                    "boot_volume_id": volume["id"],
                }
            else:
                list_fn = self._cli.block_storage.list_volume_backups
                snap_delete = self._cli.block_storage.delete_volume_backup
                vol_delete = self._cli.block_storage.delete_volume
                list_args = {
                    "volume_id": volume["id"],
                }

            snapshots = retry_on_error()(list_fn)(
                self._destination_env["compartment"], **list_args).data
            for snap in snapshots:
                retry_on_error()(snap_delete)(snap.id)

            try:
                retry_on_error()(vol_delete)(volume["id"])
            except ServiceError as err:
                if err.status == 404:
                    return
                raise

    def cleanup(self):
        self._delete_instance()
        self._delete_volumes()


class VMWareDeploymentCleanup(DeploymentCleanup):

    def __init__(self, instance_result, connection_info, destination_env):
        super(VMWareDeploymentCleanup, self).__init__(
            instance_result, connection_info, destination_env)

    def cleanup(self):
        with vmware.connect(**self._conn_info) as (_, si):
            vmware._check_delete_vm(si, self._instance_result['name'])


class OvirtDeploymentCleanup(DeploymentCleanup):

    def __init__(self, instance_result, connection_info, destination_env):
        super(OvirtDeploymentCleanup, self).__init__(
            instance_result, connection_info, destination_env)

    def _get_connection_builder(self, connection_info):
        connection_info = copy.deepcopy(connection_info)
        insecure = connection_info.pop('allow_untrusted', False)
        return ovirtsdk4.ConnectionBuilder(
            insecure=insecure,
            # NOTE: most of the debug output of the SDK are low-level
            # request/reply dumps which aren't too helpful:
            debug=False,
            **connection_info)

    @retry_on_error()
    def _delete_instance(self):
        with self._get_connection_builder(self._conn_info).build() as conn:
            vms_service = conn.system_service().vms_service()
            vm_obj = ovirtutils.find_resource_by_name_or_id(
                vms_service, self._instance_result['name'],
                raise_if_not_found=False)
            if vm_obj:
                ovirtutils.stop_vm(conn, vm_obj.id, wait=True)
                vmd = vms_service.vm_service(vm_obj.id)
                retry_on_error()(vmd.remove)()
                ovirtutils.wait_for_resource_deletion(vms_service, vm_obj.id)
            else:
                print("VM with id %s could not be found." %
                      self._instance_result["name"])

    @retry_on_error()
    def _delete_volumes(self):
        vol_ids = []
        for disk in self._instance_result["devices"]["disks"]:
            vol_ids.append(disk["id"])
        with self._get_connection_builder(self._conn_info).build() as conn:
            dks_service = conn.system_service().disks_service()
            for vol_id in vol_ids:
                disk_object = ovirtutils.find_resource_by_name_or_id(
                    dks_service, vol_id, raise_if_not_found=False)
                if disk_object:
                    disk_service = dks_service.disk_service(vol_id)
                    ovirtutils.wait_for_disk(dks_service, vol_id)
                    retry_on_error()(disk_service.remove)()
                else:
                    print("Disk with id %s previously attached to vm %s not "
                          "found." % (vol_id, self._instance_result["name"]))

    def cleanup(self):
        self._delete_instance()
        self._delete_volumes()


class LxdDeploymentCleanup(DeploymentCleanup):

    def __init__(self, instance_result, connection_info, destination_env):
        super(LxdDeploymentCleanup, self).__init__(
            instance_result, connection_info, destination_env)
        self._cli = self._get_lxd_client(conn_info=connection_info)
        self._vm = lxdutils.get_vm(
            cli=self._cli, vm_name=self._instance_result["name"])

    def _get_lxd_client(self, conn_info: LxdEnvInfo) -> pylxd.Client:
        host = conn_info['host']
        port = conn_info.get('port', 8443)

        cert = base64.b64decode(conn_info['certificate'])
        key = base64.b64decode(conn_info['key'])
        cert_dir_pattern = "lxd_client_%s_%s_" % (host, port)
        cert_dir = tempfile.mkdtemp(prefix=cert_dir_pattern)
        cert_file = os.path.join(cert_dir, "lxd.crt")
        with open(cert_file, 'wb') as fd:
            fd.write(cert)
        key_file = os.path.join(cert_dir, "lxd.key")
        with open(key_file, 'wb') as fd:
            fd.write(key)

        endpoint = "https://%s:%s" % (host, port)
        client = pylxd.Client(
            endpoint=endpoint,
            cert=(cert_file, key_file),
            verify=not conn_info.get('allow_untrusted', False))

        return client

    def _delete_instance(self):
        lxdutils.delete_vm(
            cli=self._cli, vm_name=self._instance_result["name"], force=True)

    def _delete_volumes(self):
        for disk in self._instance_result["devices"]["disks"]:
            vm_disk = self._vm.devices[disk["id"]]
            if "source" not in vm_disk:
                continue
            pool = lxdutils.get_storage_pool(
                client=self._cli, storage_pool_name=vm_disk["pool"])
            lxdutils.delete_volume(
                storage_pool=pool,
                volume_name=vm_disk["source"]
            )

    def _delete_network_forward(self):

        vm_network = lxdutils.get_vm_primary_network(
            cli=self._cli, vm=self._vm)
        lxdutils.delete_network_forward(
            network=vm_network,
            listen_address=self._instance_result["public_ip_address"])

    def cleanup(self):
        self._delete_instance()
        self._delete_volumes()
        self._delete_network_forward()


class ProxmoxDeploymentCleanup(DeploymentCleanup):

    def __init__(self, instance_result, connection_info, destination_env):
        super(ProxmoxDeploymentCleanup, self).__init__(
            instance_result, connection_info, destination_env)
        self.client = self._get_node_client(connection_info, destination_env)

    def _get_node_client(
        self,
        connection_info: dict,
        env: dict
    ) -> proxmoxutils.ProxmoxerNodeClient:
        node = env.get("import_node")
        if not node:
            raise Exception(
                "Could not find 'import_node' in 'transfer_result'.")

        return proxmoxutils.ProxmoxerNodeClient(connection_info, node)

    def cleanup(self):
        self.client.delete_vm(
            vmid=self._instance_result["id"], purge_disks=True)
