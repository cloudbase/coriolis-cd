# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import socket
import time


def get_validation_provider(public_ip_address):
    return DeploymentValidation(public_ip_address)


def _check_port_open(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.settimeout(1)
        s.connect((host, port))
        return True
    except (ConnectionRefusedError, socket.timeout, OSError):
        return False
    finally:
        s.close()


def wait_for_port_connectivity(address, port, max_wait=1200):
    i = 0
    while not _check_port_open(address, port) and i < max_wait:
        time.sleep(1)
        i += 1
    if i == max_wait:
        raise Exception("Connection failed on port %s" % port)


class DeploymentValidation(object):

    def __init__(self, public_ip_address):
        self._public_ip_address = public_ip_address

    def validate(self, ports):
        for port in ports:
            print("Checking connectivity for port: %s" % port)
            wait_for_port_connectivity(self._public_ip_address, port)
        print("All the ports connectivity successfully validated")
