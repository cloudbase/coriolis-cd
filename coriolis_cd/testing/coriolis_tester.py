# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import time

from coriolis_cd import constants
from coriolis_cd.testing.common import get_exception_details
from coriolis_cd.testing.deployment.cleanup import get_cleanup_provider
from coriolis_cd.testing.deployment.validation import get_validation_provider
from coriolis_cd import utils


class CoriolisTester(object):

    def __init__(self, coriolis_session):
        self._session = coriolis_session
        self._client = coriolis_session.get_client()

    def _wait_transfer_execution(self, transfer, execution):
        while True:
            # TODO(ibalutoiu): Workaround for not getting `TypeError: get()
            # takes exactly 3 arguments (2 given)` when calling `get()` first
            # time. The second call to `get()` works without any problem.
            try:
                execution = self._client.transfer_executions.get(
                    transfer, execution)
            except Exception:
                execution = self._client.transfer_executions.get(
                    transfer, execution)

            if execution.status in constants.ACTIVE_EXECUTION_STATUSES or (
                    execution.status == constants.EXECUTION_STATUS_UNEXECUTED):
                utils._log_msg(
                    "Transfer execution is in status: %s" % execution.status)
                time.sleep(30)
            elif execution.status in constants.FINALIZED_EXECUTION_STATUSES:
                if execution.status == constants.EXECUTION_STATUS_COMPLETED:
                    utils._log_msg("Transfer execution completed")
                    break
                else:
                    raise Exception(
                        "Transfer execution in error state: %s. Please check "
                        "the Coriolis logs for more info." % execution.status)
            else:
                raise Exception(
                    "Transfer execution is in invalid state: %s" % (
                        execution.status))
        return execution

    def _wait_deployment(self, deployment):
        while True:
            deployment = self._client.deployments.get(deployment.base_id)
            status = deployment.last_execution_status

            if status in constants.ACTIVE_EXECUTION_STATUSES or (
                    status == constants.EXECUTION_STATUS_UNEXECUTED):
                utils._log_msg("Deployment is in status: %s" % status)
                time.sleep(30)
            elif status in constants.FINALIZED_EXECUTION_STATUSES:
                if status == constants.EXECUTION_STATUS_COMPLETED:
                    utils._log_msg("Deployment completed!")
                    break
                else:
                    raise Exception(
                        "Deployment is in error state: %s. Please check the "
                        "Coriolis logs for more info." % status)
            else:
                raise Exception(
                    "Deployment is in invalid state: %s" % status)

        return deployment

    def _extract_ips_from_vm_info(self, vm_info, ipv4s_only=True):
        """ Extracts a list of possible IP addresses from the VM's export
        info as specified in the core JSONSchema definition in Coriolis:
        https://github.com/cloudbase/coriolis/blob/master/coriolis/schemas/vm_export_info_schema.json

        If present, the `vm_info['public_ip_address']` will always be first in
        the list. The rest of the IPs will be taken from
        `vm_info['devices']['nics']['ip_addresses']` in the exact order the NIC
        definitions are listed.
        """
        instance_name = vm_info.get(
            "name", vm_info.get("id", "MISSING VM NAME AND ID"))
        ip_addresses = []

        public_ip = vm_info.get("public_ip_address", "")
        if public_ip:
            if ipv4s_only and not utils.validate_ipv4_address(public_ip):
                utils._log_msg(
                    "WARN: the 'public_ip_address' ('%s') of instance '%s' "
                    "is NOT an IPv4. Skipping. VM info was: %s" % (
                        public_ip, instance_name, vm_info))
            else:
                ip_addresses.append(public_ip)

        nics = vm_info.get("devices", {}).get("nics", [])
        if not nics:
            utils._log_msg(
                "WARN: instance '%s' export/transfer info does "
                "NOT contain any NIC definitions. Returning IP "
                "address list %s. Export info was: %s" % (
                    instance_name, ip_addresses, vm_info))
            return ip_addresses
        for nic_idx, nic in enumerate(
                vm_info.get("devices", {}).get("nics", [])):
            nic_ips = nic.get("ip_addresses", [])

            for nic_ip in nic_ips:
                if ipv4s_only and not utils.validate_ipv4_address(nic_ip):
                    utils._log_msg(
                        "WARN: one of the 'ip_addresses' of NIC %d ('%s') for "
                        "instance '%s' is NOT an IPv4. Skipping. VM info was: "
                        "%s" % (
                            nic_idx, nic_ip, instance_name, vm_info))
                else:
                    ip_addresses.append(nic_ip)

        utils._log_msg(
            "Extracted IP addresses %s for instance '%s' from VM export "
            "info: %s" % (ip_addresses, instance_name, vm_info))
        return ip_addresses

    def _get_deployed_vm_ip_addresses(self,
                                      instance_result,
                                      deployment,
                                      ipv4s_only=True,
                                      max_retries=10,
                                      retry_period=30):
        """ Attempts to extract the IP address(es) of the instance transfered
        with the given `transfer_result`.

        If the transfer_result is missing/empty, it will attempt to
        `coriolis endpoint instance show` the info for the deployed VM
        on the target platform (assuming the target provider is also
        capable of listing instances, which is not a given).
        """
        instance_name = instance_result.get(
            "name", instance_result.get("id", "MISSING VM NAME AND ID"))
        ip_addresses = []
        if instance_result:
            ip_addresses = self._extract_ips_from_vm_info(
                instance_result, ipv4s_only=ipv4s_only)

            if ip_addresses:
                utils._log_msg(
                    "Successfully determined following IP addresses "
                    "for instance '%s' from its transfer_result: %s" % (
                        instance_name, ip_addresses))
                return ip_addresses

        utils._log_msg(
            "WARN: failed to extract IP addresses from transfer_result "
            "for instance '%s' of deployment '%s'. Attempting to fetch "
            "the deployed instance's IPs through the Coriolis instance "
            "listing API. transfer_result was: %s" % (
                instance_name, deployment.id, instance_result))

        i = 0
        vm_data = None
        vm_data_dict = {}
        while not ip_addresses and i < max_retries:
            utils._log_msg(
                "(%d/%d) Trying to fetch IP address for '%s' from "
                "destination platform using destination provider "
                "instance listing API" % (
                    i + 1, max_retries, instance_name))
            try:
                vm_data = self._client.endpoint_instances.get(
                    instance_id=instance_name,
                    endpoint=deployment.destination_endpoint_id)
                vm_data_dict = vm_data.to_dict()
            except Exception:
                utils._log_msg("Could not get VM details from destination "
                               "endpoint. Error was: %s\n" % (
                                   utils.get_exception_details()))
                raise

            ip_addresses = self._extract_ips_from_vm_info(
                vm_data_dict, ipv4s_only=ipv4s_only)
            if ip_addresses:
                utils._log_msg(
                    "Successfully fetched following IP addresses "
                    "for instance '%s' via endpoint instance listing "
                    "API after %d tries: %s" % (
                        instance_name, i + 1, ip_addresses))
                break

            utils._log_msg(
                "(%d/%d) Waiting %d seconds to refresh IP address for '%s' "
                "from destination platform using destination provider "
                "instance listing API" % (
                    i + 1, max_retries, retry_period, instance_name))
            i += 1
            time.sleep(retry_period)

        utils._log_msg(
            "IP addresses of deployed VM '%s' from deployment '%s' "
            "are: %s" % (instance_name, deployment.id, ip_addresses))
        return ip_addresses

    def create_transfer(self,
                        transfer_scenario,
                        origin_endpoint_name,
                        destination_endpoint_name,
                        source_environment={},
                        destination_environment={},
                        network_map={},
                        storage_mappings={},
                        instances=[]):
        origin_endpoint_id = self._client.endpoints.get_endpoint_id_for_name(
            endpoint=origin_endpoint_name)
        dest_endpoint_id = self._client.endpoints.get_endpoint_id_for_name(
            endpoint=destination_endpoint_name)

        return self._client.transfers.create(
            transfer_scenario=transfer_scenario,
            origin_endpoint_id=origin_endpoint_id,
            destination_endpoint_id=dest_endpoint_id,
            source_environment=source_environment,
            destination_environment=destination_environment,
            instances=instances,
            network_map=network_map,
            storage_mappings=storage_mappings,
            user_scripts={})

    def run_transfer(self, transfer):
        execution = self._client.transfer_executions.create(transfer)

        return self._wait_transfer_execution(transfer, execution)

    def run_deployment_from_transfer(self, transfer_id):
        deployment = self._client.deployments.create_from_transfer(
            transfer_id=transfer_id, user_scripts={})

        return self._wait_deployment(deployment)

    def cleanup_transfer_disks(self, transfer, fatal=True):
        try:
            execution = self._client.transfers.delete_disks(transfer)
            return self._wait_transfer_execution(transfer, execution)
        except Exception:
            if fatal:
                raise
            utils._log_msg(
                "Failed to cleanup disks for transfer '%s'.Exception "
                "details:\n%s" % (transfer.id, get_exception_details()))

    def validate_deployment(self, deployment, ports=None):
        """ Iterates over every VM which was part of the given Deployment
        and attempts to validate the deployed VM by attempting to connect
        to any one of its IP addresses on the given ports.
        """

        if len(ports) == 0:
            utils._log_msg(
                "WARN: The validation ports list for deployment "
                "with ID '%s' is empty. Skipping" % (deployment.id))
            return

        transfer_result = deployment.transfer_result.to_dict()
        for vm_name in deployment.instances:
            instance_result = transfer_result.get(vm_name, {})
            if not instance_result:
                raise Exception(
                    "No 'transfer_result' dict present for VM '%s' in "
                    "deployment '%s': %s" % (
                        vm_name, deployment.id, transfer_result))

            ip_addresses = self._get_deployed_vm_ip_addresses(
                instance_result=instance_result,
                deployment=deployment,
                ipv4s_only=True)
            if not ip_addresses:
                raise Exception(
                    "Failed to determine IP addresses for deployed VM '%s' "
                    "from deployment '%s' from transfer_result: %s" % (
                        vm_name, deployment.id, transfer_result))

            was_validated = False
            for ip_address in ip_addresses:
                try:
                    validation_provider = get_validation_provider(ip_address)
                    utils._log_msg(
                        "Attempting to check deployed VM '%s' from deployment "
                        "'%s' on IP address: '%s'" % (
                            vm_name, deployment.id, ip_address))
                    validation_provider.validate(ports=ports)

                    utils._log_msg(
                        "Successfully validated deployment of VM '%s' "
                        "from deployment '%s' on IP address '%s' and ports "
                        "%s" % (
                            vm_name, deployment.id, ip_addresses, ports))
                    was_validated = True
                    break
                except Exception:
                    utils._log_msg(
                        "Failed to validate deployment of VM '%s' "
                        "from deployment '%s' on IP address '%s' and ports "
                        "%s. Trying next IP. Error was: %s" % (
                            vm_name, deployment.id, ip_addresses, ports,
                            utils.get_exception_details()))

            if not was_validated:
                raise Exception(
                    "Failed to validate deployment of VM '%s' "
                    "from deployment '%s' on any of its addresses (%s) and "
                    "ports %s." % (
                        vm_name, deployment.id, ip_addresses, ports))

    def cleanup_deployment(self, deployment, fatal=True):
        try:
            dest_endpoint = self._client.endpoints.get(
                deployment.destination_endpoint_id)
            conn_info = self._session.get_secret_connection_info(
                dest_endpoint.connection_info.to_dict()
            )
            dest_env = deployment.destination_environment.to_dict()
            transfer_result = deployment.transfer_result.to_dict()
            for vm_name in deployment.instances:
                instance_result = transfer_result[vm_name]
                cleanup_provider = get_cleanup_provider(
                    provider_type=dest_endpoint.type,
                    connection_info=conn_info,
                    destination_env=dest_env,
                    instance_result=instance_result)
                cleanup_provider.cleanup()
        except Exception:
            if fatal:
                raise
            utils._log_msg(
                "Failed to cleanup deployment '%s'. Exception details:\n%s" % (
                    deployment.id, get_exception_details()))
