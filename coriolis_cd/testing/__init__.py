# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

from coriolis_cd import schemas

TESTING_CONFIG_SCHEMA = schemas.get_schema(
        __name__, schemas._TESTING_CONFIG_SCHEMA_NAME)
