# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

from coriolis_cd import constants
from coriolis_cd import utils
from coriolis_cd.operations import common


def _run_coriolis_build(
        ssh_client, local_ssh_key_path, remote_ssh_key_path,
        registry, registry_username, registry_password,
        export_list, import_list,
        coriolis_docker_repository_url, coriolis_docker_branch_name,
        custom_repo_names={}, custom_branch_names={},
        release_tag=constants.DEFAULT_RELEASE_TAG):

    common.login_remote_docker_repository(
        ssh_client, registry, registry_username, registry_password)

    common.clone_remote_git_repo(
        ssh_client,
        coriolis_docker_repository_url,
        coriolis_docker_branch_name,
        common.CORIOLIS_DOCKER_REPO_PATH)

    ssh_cmd = "source %s/utils/common.sh && new_config_file $%s" % (
        common.CORIOLIS_DOCKER_REPO_PATH, common.MAIN_CONFIG)
    utils._exec_ssh_cmd(ssh_client, ssh_cmd)

    ssh_cmd = "source %s/utils/common.sh && new_config_file $%s" % (
        common.CORIOLIS_DOCKER_REPO_PATH, common.DOCKER_CONFIG)
    utils._exec_ssh_cmd(ssh_client, ssh_cmd)

    common.update_remote_config_file(
        ssh_client,
        common.MAIN_CONFIG,
        new_config_options={"bitbucket_ssh_key_file": remote_ssh_key_path,
                            "coriolis_export_providers": export_list,
                            "coriolis_import_providers": import_list,
                            "custom_repo_owner_names": custom_repo_names,
                            "custom_repo_branch_names": custom_branch_names,
                            "enable_coriolis_licensing_server": True})

    common.update_remote_config_file(
        ssh_client,
        common.DOCKER_CONFIG,
        new_config_options={"docker_pull_images": False,
                            "default_coriolis_docker_images_tag": release_tag})

    utils._log_msg("Bootstrap Coriolis appliance")
    utils._exec_ssh_cmd(
        ssh_client,
        "%s/coriolis-ansible bootstrap" % common.CORIOLIS_DOCKER_REPO_PATH)

    utils._log_msg("Upload Bitbucket SSH key")
    utils._send_file(ssh_client, local_ssh_key_path, remote_ssh_key_path)
    utils._exec_ssh_cmd(ssh_client, "chmod 600 %s" % remote_ssh_key_path)

    utils._log_msg("Build Coriolis Docker images")
    utils._exec_ssh_cmd(
        ssh_client,
        "%s/coriolis-ansible build" % common.CORIOLIS_DOCKER_REPO_PATH)

    utils._log_msg("Remove remote SSH private key")
    utils._exec_ssh_cmd(ssh_client, "rm %s" % remote_ssh_key_path)

    common.remote_cleanup(ssh_client, registry)
