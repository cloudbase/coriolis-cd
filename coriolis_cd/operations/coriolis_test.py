#!/usr/bin/env python3

import json

from coriolis_cd import schemas
from coriolis_cd import testing

from coriolis_cd.testing import common
from coriolis_cd.testing.coriolis_tester import CoriolisTester


def _cleanup_transfer(cleanup, tester, transfer):
    if not cleanup:
        return
    if not transfer:
        return
    print("Cleaning up the transfer disks")
    tester.cleanup_transfer_disks(transfer)


def _cleanup_deployment(cleanup, tester, deployment):
    if not cleanup:
        return
    if not deployment:
        return
    print("Cleaning up the migrated resources from the dest env")
    tester.cleanup_deployment(deployment)


def run_test(source_environment, destination_environment,
             default_storage_backend, auth_url, user_name,
             user_password, project_name, user_domain_name,
             project_domain_name, origin_endpoint, destination_endpoint,
             network_map, instances, test_scenario, validation_ports, cleanup,
             verify=True):
    source_env = {}
    if source_environment:
        source_env = json.loads(source_environment)
    destination_env = {}
    if destination_environment:
        destination_env = json.loads(destination_environment)
    storage_mappings = {}
    if default_storage_backend:
        storage_mappings["default"] = default_storage_backend

    coriolis_session = common.CoriolisSession(
        auth_url=auth_url,
        username=user_name,
        password=user_password,
        project_name=project_name,
        user_domain_name=user_domain_name,
        project_domain_name=project_domain_name,
        verify=verify)

    tester = CoriolisTester(coriolis_session)

    coriolis_params = {
        "transfer_scenario": test_scenario,
        "origin_endpoint_name": origin_endpoint,
        "destination_endpoint_name": destination_endpoint,
        "source_environment": source_env,
        "destination_environment": destination_env,
        "network_map": json.loads(network_map),
        "storage_mappings": storage_mappings,
        "instances": instances
    }

    transfer = None
    deployment = None

    try:
        transfer = tester.create_transfer(**coriolis_params)
        tester.run_transfer(transfer)
        print(f"Running incremental run for transfer: {transfer.base_id}")
        tester.run_transfer(transfer)
        deployment = tester.run_deployment_from_transfer(transfer.base_id)
        tester.validate_deployment(deployment, validation_ports)
    finally:
        _cleanup_deployment(cleanup, tester, deployment)
        _cleanup_transfer(cleanup, tester, transfer)


def check_config(config_yaml_as_dict):
    config = json.dumps(config_yaml_as_dict)
    schemas.validate_string(config, testing.TESTING_CONFIG_SCHEMA)
    print("Successful schema validation")

    config = json.loads(config)

    for key in ["endpoints", "export_providers", "import_providers", "tests"]:
        if len(config[key]) == 0:
            raise Exception("Empty '%s' list" % (key))

    endpoints_names = []
    for endpoint in config['endpoints']:
        if not endpoint['name']:
            raise Exception("Empty endpoint name")
        if not endpoint['connection_info']:
            raise Exception("Empty connection info for endpoint '%s'" % (
                endpoint['name']))
        if endpoint['name'] in endpoints_names:
            raise Exception("Duplicate endpoint name found: '%s'" % (
                endpoint['name']))
        endpoints_names.append(endpoint['name'])

    e_providers_ids = []
    for e_provider in config['export_providers']:
        for key in ["id", "endpoint_name", "network_name"]:
            if not e_provider[key]:
                raise Exception("Empty '%s' field for the export provider" % (
                    key))
        if e_provider['id'] in e_providers_ids:
            raise Exception("Duplicate export provider id found: '%s'" % (
                e_provider['id']))
        if e_provider['endpoint_name'] not in endpoints_names:
            raise Exception("The export provider '%s' uses non-existent "
                            "endpoint: '%s'" % (e_provider['id'],
                                                e_provider['endpoint_name']))
        e_providers_ids.append(e_provider['id'])

    i_providers_ids = []
    for i_provider in config['import_providers']:
        for key in ["id", "network_name", "env_info"]:
            if not i_provider[key]:
                raise Exception("Empty '%s' field for the import provider" % (
                    key))
        if i_provider['id'] in i_providers_ids:
            raise Exception("Duplicate import provider id found: '%s'" % (
                i_provider['id']))
        if i_provider['endpoint_name'] not in endpoints_names:
            raise Exception("The import provider '%s' uses non-existent "
                            "endpoint: '%s'" % (i_provider['id'],
                                                i_provider['endpoint_name']))
        i_providers_ids.append(i_provider['id'])

    for test in config['tests']:
        if test['export_provider_id'] not in e_providers_ids:
            raise Exception("A test uses non-existent export provider "
                            "id: '%s'" % (test['export_provider_id']))
        if test['import_provider_id'] not in i_providers_ids:
            raise Exception("A test uses non-existent import provider "
                            "id: '%s'" % (test['import_provider_id']))
        if not test["vm_name"]:
            raise Exception("Empty 'vm_name' field for the '%s' test from "
                            "'%s' to '%s'" % (test['scenario'],
                                              test['export_provider_id'],
                                              test['import_provider_id']))

    print("Successful testing config validation")
