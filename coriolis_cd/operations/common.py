# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import os
import tempfile
import yaml

from coriolis_cd import utils


CORIOLIS_DOCKER_REPO_PATH = "/root/coriolis-docker"
MAIN_CONFIG = 'CONFIG_FILE'
DOCKER_CONFIG = 'DOCKER_IMAGES_CONFIG_FILE'


def login_remote_docker_repository(
        ssh_client, registry, registry_username, registry_password):
    utils._log_msg("Log into Docker registry: '%s'" % registry)
    utils._exec_ssh_cmd(
        ssh_client,
        "docker login %s -u %s -p '%s'" % (
            registry, registry_username, registry_password))


def clone_remote_git_repo(
        ssh_client, git_repo_url, git_branch_name, git_repo_path):
    utils._exec_ssh_cmd(
        ssh_client,
        "[[ -e %s ]] || git clone %s --branch %s" % (git_repo_path,
                                                     git_repo_url,
                                                     git_branch_name))


def update_remote_config_file(
        ssh_client, file_to_update, new_config_options={}):
    if len(new_config_options) == 0:
        return

    utils._log_msg("Updating config file '%s' with options '%s'" % (
        file_to_update, new_config_options))

    remote_config_file_path = utils._exec_ssh_cmd(
        ssh_client,
        "source %s/utils/common.sh && echo -n $%s" % (
            CORIOLIS_DOCKER_REPO_PATH, file_to_update))

    config = yaml.safe_load(
        utils._exec_ssh_cmd(ssh_client, "cat %s" % remote_config_file_path))

    for key in new_config_options.keys():
        config[key] = new_config_options[key]

    fd, file_path = tempfile.mkstemp()
    try:
        with os.fdopen(fd, 'w') as tmp:
            tmp.write(yaml.safe_dump(config, default_flow_style=False))
        utils._send_file(ssh_client, file_path, remote_config_file_path)
    finally:
        os.remove(file_path)


def remote_cleanup(ssh_client, registry):
    utils._log_msg("Clean up and log out")
    utils._exec_ssh_cmd(
        ssh_client,
        "if [[ -e ~/.bash_history ]]; then rm ~/.bash_history; fi")
    utils._exec_ssh_cmd(ssh_client, "docker logout")
    utils._exec_ssh_cmd(ssh_client, "docker logout %s" % registry)
