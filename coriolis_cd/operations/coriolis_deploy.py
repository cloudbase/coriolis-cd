# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

from coriolis_cd import utils
from coriolis_cd.operations import common


def _run_coriolis_deployment(
        ssh_client, pull_images,
        registry, registry_username, registry_password,
        coriolis_docker_repository_url, coriolis_docker_branch_name):

    common.login_remote_docker_repository(
        ssh_client, registry, registry_username, registry_password)

    common.clone_remote_git_repo(
        ssh_client,
        coriolis_docker_repository_url,
        coriolis_docker_branch_name,
        common.CORIOLIS_DOCKER_REPO_PATH)

    ssh_cmd = "source %s/utils/common.sh && new_config_file $%s" % (
        common.CORIOLIS_DOCKER_REPO_PATH, common.MAIN_CONFIG)
    utils._exec_ssh_cmd(ssh_client, ssh_cmd)

    ssh_cmd = "source %s/utils/common.sh && new_config_file $%s" % (
        common.CORIOLIS_DOCKER_REPO_PATH, common.DOCKER_CONFIG)
    utils._exec_ssh_cmd(ssh_client, ssh_cmd)

    common.update_remote_config_file(
        ssh_client,
        common.MAIN_CONFIG,
        new_config_options={"enable_coriolis_licensing_server": True})

    common.update_remote_config_file(
        ssh_client,
        common.DOCKER_CONFIG,
        new_config_options={"docker_pull_images": pull_images})

    utils._log_msg("Bootstrap Coriolis appliance")
    utils._exec_ssh_cmd(
        ssh_client,
        "%s/coriolis-ansible bootstrap" % common.CORIOLIS_DOCKER_REPO_PATH)

    utils._log_msg("Deploy Kolla")
    utils._exec_ssh_cmd(
        ssh_client,
        "%s/kolla/deploy.sh" % common.CORIOLIS_DOCKER_REPO_PATH)

    utils._log_msg("Deploy Coriolis")
    utils._exec_ssh_cmd(
        ssh_client,
        "%s/coriolis-ansible deploy" % common.CORIOLIS_DOCKER_REPO_PATH)

    utils._log_msg("Reset Coriolis licensing DB")
    utils._exec_ssh_cmd(
        ssh_client,
        "%s/coriolis-ansible reset-licensing-db" % (
            common.CORIOLIS_DOCKER_REPO_PATH))

    common.remote_cleanup(ssh_client, registry)
