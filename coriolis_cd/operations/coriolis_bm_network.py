# Copyright 2023 Cloudbase Solutions Srl
# All Rights Reserved.

from coriolis_cd.testing import common


@common.retry_on_error()
def get_bm_network(bm_id, bm_endpoint, auth_url, user_name, user_password,
                   project_name, user_domain_name, project_domain_name,
                   verify=True):
    coriolis_client = common.CoriolisSession(
        auth_url=auth_url,
        username=user_name,
        password=user_password,
        project_name=project_name,
        user_domain_name=user_domain_name,
        project_domain_name=project_domain_name,
        verify=verify).get_client()

    mh_endpoint_id = coriolis_client.endpoints.get_endpoint_id_for_name(
        bm_endpoint)
    bm_info = coriolis_client.endpoint_instances.get(
        endpoint=mh_endpoint_id, instance_id=bm_id)
    bm_info_dict = bm_info.to_dict()

    columns = ('BM Network',)
    data = (bm_info_dict['devices']['nics'][0]['network_name'],)

    return columns, data
