# Copyright 2023
# Cloudbase Solutions Srl
# All Rights Reserved.

import json

from cliff.show import ShowOne

from coriolis_cd import providers
from coriolis_cd import schemas
from coriolis_cd import utils
from coriolis_cd.providers import base


class BaremetalMachineDeploy(ShowOne):
    """Deploy and configure VM from template for BareMetal tests"""

    def get_parser(self, prog_name):
        parser = super(BaremetalMachineDeploy, self).get_parser(prog_name)
        parser.add_argument('--platform', dest="provider", required=True,
                            help='The type of the platform the VM is deployed')
        parser.add_argument('--connection-info', dest="connection_info",
                            required=True,
                            help='Dictionary containing connection info in '
                                 'JSON format.')
        parser.add_argument('--vm-config', dest="vm_configuration",
                            required=True,
                            help='VM configuration for BareMetal test setup.')
        parser.add_argument('--appliance-config',
                            dest="appliance_configuration", required=True,
                            help='Dictionary containing Coriolis appliance '
                                 'details.')
        return parser

    def take_action(self, args):
        if not args.vm_configuration:
            raise Exception("VM configuration is a mandatory parameter.")
        if not args.connection_info:
            raise Exception("Connection information is a mandatory parameter.")
        if not args.appliance_configuration:
            raise Exception("Coriolis Appliance configuration file is a "
                            "mandatory parameter.")

        conn_info = json.loads(args.connection_info)
        vm_config = json.loads(args.vm_configuration)
        appliance_config = json.loads(args.appliance_configuration)

        try:
            baremetal_provider = utils.get_provider(
                args.provider,
                base.BaseBareMetalProvider,
                providers.PROVIDER_LIST)
        except BaseException:
            utils._log_msg(
                "Exception occured while loading BareMetal provider:\n%s" % (
                    utils.get_exception_details()))
            raise

        schemas.validate_value(
            conn_info, baremetal_provider.conn_info_schema)

        schemas.validate_value(
            vm_config, baremetal_provider.vm_config_schema)

        schemas.validate_value(
            appliance_config, baremetal_provider.appliance_config_schema)

        return baremetal_provider.deploy_bare_metal_machine(
            conn_info, vm_config, appliance_config)
