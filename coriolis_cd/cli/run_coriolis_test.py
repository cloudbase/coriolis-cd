# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

from cliff.command import Command

from coriolis_cd.operations.coriolis_test import run_test as run_coriolis_test


class RunCoriolisTest(Command):
    """Run a coriolis replica/migration test"""

    def get_parser(self, prog_name):
        parser = super(RunCoriolisTest, self).get_parser(prog_name)
        parser.add_argument("--test-scenario", type=str, required=True,
                            choices=["replica", "live_migration"],
                            default="replica",
                            help="The type of Coriolis CI test executed")
        parser.add_argument("--origin-endpoint", type=str, required=True,
                            help="The origin endpoint name")
        parser.add_argument("--destination-endpoint", type=str, required=True,
                            help="The destination endpoint name")
        parser.add_argument("--source-environment",
                            help="JSON encoded data related to the "
                            "source's environment")
        parser.add_argument("--destination-environment",
                            help="JSON encoded data related to the "
                            "destination's environment")
        parser.add_argument("--network-map", required=True,
                            help="JSON mapping between identifiers of "
                            "networks on the source and identifiers of "
                            "networks on the destination")
        parser.add_argument("--default-storage-backend",
                            help="Name of a storage backend on the destination"
                            " platform to default to using")
        parser.add_argument("--instance", action="append", required=True,
                            dest="instances",
                            help="Instances to be replicated, can be "
                            "specified multiple times")
        parser.add_argument("--validation-port", type=int, action="append",
                            default=[], dest="validation_ports",
                            help="The TCP ports being checked for connectivity"
                            ", after the VM is successfully migrated.")
        parser.add_argument("--cleanup", action="store_true",
                            dest="cleanup",
                            help="This flag enables cleanup for the deployment"
                            " resources and the transfer disks from the "
                            "destination environment.")
        parser.add_argument("--insecure",
                            action='store_true', default=False,
                            help="Skip server TLS certificate verification",
                            required=False)
        parser.add_argument("--ca-cert",
                            type=str,
                            help="Coriolis CA certificate file",
                            required=False)
        parser.add_argument("--auth-url",
                            type=str,
                            help="Coriolis Keystone auth-url",
                            required=True)
        parser.add_argument("--user-name",
                            type=str,
                            help="Coriolis Keystone username",
                            required=True)
        parser.add_argument("--user-password",
                            type=str,
                            help="Coriolis Keystone user password",
                            required=True)
        parser.add_argument("--project-name", default="admin",
                            type=str,
                            help="Coriolis Keystone project name")
        parser.add_argument("--user-domain-name", default="Default",
                            type=str,
                            help="Coriolis Keystone user domain name")
        parser.add_argument("--project-domain-name", default="Default",
                            type=str,
                            help="Coriolis Keystone project domain name")
        return parser

    def take_action(self, args):
        run_coriolis_test(args.source_environment,
                          args.destination_environment,
                          args.default_storage_backend,
                          args.auth_url, args.user_name,
                          args.user_password,
                          args.project_name, args.user_domain_name,
                          args.project_domain_name, args.origin_endpoint,
                          args.destination_endpoint, args.network_map,
                          args.instances, args.test_scenario,
                          args.validation_ports, args.cleanup,
                          verify=(args.ca_cert or not args.insecure))
