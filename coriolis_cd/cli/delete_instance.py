# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import json

from cliff.show import ShowOne

from coriolis_cd import providers
from coriolis_cd import schemas
from coriolis_cd import utils


class DeleteInstance(ShowOne):
    """Delete an instance"""

    def get_parser(self, prog_name):
        parser = super(DeleteInstance, self).get_parser(prog_name)
        parser.add_argument('--platform', dest="provider", required=True,
                            help='The type of the platform the VM is deleted from')  # noqa: E501
        parser.add_argument('--connection-info', dest="connection_info",
                            required=True,
                            help='Dictionary containing connection info in JSON format')  # noqa: E501
        parser.add_argument('--environment-info', dest='environment_info',
                            required=True,
                            help='Environment-specific details in JSON format')
        parser.add_argument('--vm-identifier', dest="vm_name", required=True,
                            help="String identifier for the VM to be deleted")
        return parser

    def take_action(self, args):
        if not args.vm_name:
            raise Exception("Invalid VM identifier")
        columns = None
        data = None
        conn_info = json.loads(args.connection_info)
        env_info = json.loads(args.environment_info)
        try:
            boot_provider = utils.get_provider(
                args.provider,
                providers.base.BaseApplianceTemplateBootProvider,
                providers.PROVIDER_LIST)
        except BaseException:
            utils._log_msg("Exception occured while loading provider:\n%s" % (
                utils.get_exception_details()))
            raise

        schemas.validate_value(
            conn_info, boot_provider.conn_info_schema)

        schemas.validate_value(
            env_info, boot_provider.env_info_schema)

        columns, data = boot_provider.delete_instance(
            conn_info, env_info, args.vm_name)
        return (columns, data)
