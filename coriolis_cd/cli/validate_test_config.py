# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import yaml

from cliff.command import Command

from coriolis_cd.operations.coriolis_test import check_config


class ValidateTestConfig(Command):
    """Validate a coriolis-ci test config"""

    def get_parser(self, prog_name):
        parser = super(ValidateTestConfig, self).get_parser(prog_name)
        parser.add_argument('--config-file-path', dest="config", required=True,
                            help='The path to the config file in yaml format.')
        return parser

    def take_action(self, args):
        if not args.config:
            raise Exception("Invalid config path")

        with open(args.config) as f:
            test_config = yaml.safe_load(f)

        check_config(test_config)
