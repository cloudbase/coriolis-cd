# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

from cliff.command import Command

from coriolis_cd import utils


class ExposeCoriolis(Command):
    """Runs coriolis-docker/expose_coriolis.py on the VM"""

    def get_parser(self, prog_name):
        parser = super(ExposeCoriolis, self).get_parser(prog_name)
        parser.add_argument('--host', dest="host", required=True,
                            help='Hostname/address of the VM')
        parser.add_argument('--username', dest="username", required=True,
                            help='VM username.')
        parser.add_argument('--password', dest="password",
                            help='VM password', required=True)
        return parser

    def take_action(self, args):
        vm_ssh = utils._get_ssh_client(args.host, args.username, args.password)

        utils._exec_ssh_cmd(vm_ssh,
                            "~/coriolis-docker/expose_coriolis.py")
