import abc


class BaseApplianceTemplateBootProvider(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def boot_appliance_template(self, conn_info, env_info, vm_identifier):
        """
        param conn_info: dict: dict with connection info for the platform
        example: host, user, pass for a vmware env

        param env_info: dict: parameters relating to the env
        example: resource group on Azure, template name on VMWare, dc name on
        VMWare
        """
        pass

    @abc.abstractmethod
    def delete_instance(self, conn_info, env_info, vm_identifier):
        """
        param conn_info: dict: dict with connection info for the platform
        example: host, user, pass for a vmware env

        param env_info: dict: parameters relating to the env
        example: resource group on Azure, template name on VMWare, dc name on
        VMWare
        """
        pass

    @abc.abstractmethod
    def boot_appliance_from_url(self, conn_info, env_info, vm_identifier,
                                appliance_url):
        """
        param conn_info: dict: dict with connection info for the platform
        example: host, user, pass for a vmware env

        param env_info: dict: parameters relating to the env
        example: resource group on Azure, template name on VMWare, dc name on
        VMWare

        param appliance_url: string: URL pointing to an all-in-one appliance in
        a platform-compatible format
        """
        pass


class BaseApplianceExportProvider(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def export_appliance(self, conn_info, env_info, vm_identifier,
                         export_path):
        """
        param conn_info: dict: dict with connection info for the platform
        example: host, user, pass for a vmware env

        param env_info: dict: parameters relating to the env
        example: resource group on Azure, template name on VMWare
        """
        pass


class BaseBareMetalProvider(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def deploy_bare_metal_machine(self, conn_info, env_info, vm_identifier,
                                  vm_config):
        """
        param conn_info: dict: dict with connection info for the platform
        example: host, user, pass for a vmware env

        param env_info: dict: parameters relating to the env
        example: resource group on Azure, template name on VMWare

        param vm_config: dict: dict with username, password, name, template,
        datacenter name and Coriolis Snapshot Agent URL for the VM acting as BM
        example: username, password, name, template, datacenter, agent_url
        """
        pass

    @abc.abstractmethod
    def  bare_metal_machine_register(self, conn_info, vm_config,
                                     appliance_config):
        """
        param conn_info: dict: dict with connection info for the platform
        example: host, user, pass for a vmware env

        param vm_config: dict: dict with username, password, name, template,
        datacenter name and Coriolis Snapshot Agent URL for the VM acting as BM
        example: username, password, name, template, datacenter, agent_url

        param appliance_config: dict: dict with host, username and password
        of Coriolis Appliance
        example: host, user, password
        """
        pass

    @abc.abstractmethod
    def  bare_metal_machine_delete(self, conn_info, vm_config):
        """
        param conn_info: dict: dict with connection info for the platform
        example: host, user, pass for a vmware env

        param vm_config: dict: dict with username, password, name, template,
        datacenter name and Coriolis Snapshot Agent URL for the VM acting as BM
        example: username, password, name, template, datacenter, agent_url
        """
        pass
