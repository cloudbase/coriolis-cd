from coriolis_cd.providers.vmware import provider as vmware

PROVIDER_LIST = [vmware.ApplianceTemplateBootProvider,
                 vmware.ApplianceExportProvider,
                 vmware.BareMetalProvider]
