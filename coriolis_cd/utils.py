# Copyright 2018 Cloudbase Solutions Srl
# All Rights Reserved.
import ipaddress
import traceback

import os
import paramiko


def _log_msg(msg):
    """ Wrapper to log message. """
    print(msg)


def _get_ssh_client(ip, vm_username, vm_password):
    _log_msg("Connecting to host: '%s'" % ip)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(
        paramiko.AutoAddPolicy())
    ssh.connect(ip, username=vm_username, password=vm_password)
    return ssh


def _exec_ssh_cmd(ssh, cmd, check_exit_code=True):
    stdin, stdout, stderr = ssh.exec_command(cmd)
    std_out = stdout.read().decode(errors='replace')
    std_err = stderr.read().decode(errors='replace')
    _log_msg(std_out)
    _log_msg(std_err)
    exit_code = stdout.channel.recv_exit_status()
    if check_exit_code and exit_code != 0:
        raise Exception("Command exited with code: %s" % exit_code)
    return std_out

def _exec_ssh_cmd_sourced(ssh, cmd, check_exit_code=True):
    stdin, stdout, stderr = ssh.exec_command(
        "source /etc/kolla/admin-openrc.sh; source /root/metalhubrc; %s" % cmd)
    std_out = stdout.read().decode(errors='replace')
    std_err = stderr.read().decode(errors='replace')
    _log_msg(std_out)
    _log_msg(std_err)
    exit_code = stdout.channel.recv_exit_status()
    if check_exit_code and exit_code != 0:
        raise Exception("Command exited with code: %s" % exit_code)
    return std_out

def _send_file(ssh, localfile, remotefile):
    transport = ssh.get_transport()
    sftp_client = transport.open_sftp_client()
    sftp_client.put(localfile, remotefile)

def _get_file(ssh, remotefile, localfile):
    transport = ssh.get_transport()
    sftp_client = transport.open_sftp_client()
    sftp_client.get(remotefile, localfile)

def get_provider(platform_name, provider_type, provider_list,
                 raise_on_not_found=True):
    for prov in provider_list:
        if prov.platform == platform_name:
            if issubclass(prov, provider_type):
                return prov()
    if raise_on_not_found:
        raise Exception("Could not load provider %s of type %s." % (
            platform_name, provider_type))


def get_exception_details():
    return traceback.format_exc()


def validate_ipv4_address(ip_address):
    try:
        ipaddress.IPv4Address(ip_address)
        return True
    except Exception:
        return False


def get_appliance_fingerprint(ssh):
    temp_file="/tmp/fingerprint"
    fingerprint_file="/etc/step/fingerprint"
    _get_file(ssh, fingerprint_file, temp_file)

    fingerprint = None
    with open(temp_file, "r") as f:
        fingerprint = f.readline().rstrip('\n')
    return fingerprint
