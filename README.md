# Coriolis appliance deployment/delivery tools #
This tool automates the process of building a coriolis appliance.


Booting a base appliance VM:

    usage: coriolis-cd boot base appliance [-h] [-f {json,shell,table,value,yaml}]
                                       [-c COLUMN] [--noindent]
                                       [--prefix PREFIX]
                                       [--max-width <integer>] [--fit-width]
                                       [--print-empty] --provider PROVIDER
                                       --connection-info CONNECTION_INFO
                                       --environment-info ENVIRONMENT_INFO
                                       --vm-identifier VM_NAME

Deleting a VM:

    usage: coriolis-cd delete instance [-h] [-f {json,shell,table,value,yaml}]
                                   [-c COLUMN] [--noindent] [--prefix PREFIX]
                                   [--max-width <integer>] [--fit-width]
                                   [--print-empty] --provider PROVIDER
                                   --connection-info CONNECTION_INFO
                                   --environment-info ENVIRONMENT_INFO
                                   --vm-identifier VM_NAME

Exporting a VM:

    usage: coriolis-cd export instance [-h] [-f {json,shell,table,value,yaml}]
                                   [-c COLUMN] [--noindent] [--prefix PREFIX]
                                   [--max-width <integer>] [--fit-width]
                                   [--print-empty] --provider PROVIDER
                                   --connection-info CONNECTION_INFO
                                   --environment-info ENVIRONMENT_INFO
                                   --vm-identifier VM_NAME --target-path
                                   TARGET_PATH

Building the Coriolis components on a VM:

    usage: coriolis-cd build coriolis components [-h] --host HOST
                                             --username USERNAME --password PASSWORD
                                             --key-path KEY_PATH
                                             --remote-key-path REM_KEY_PATH
                                             --docker-registry REGISTRY_HOST
                                             --docker-registry-user REGISTRY_USERNAME
                                             --docker-registry-password REGISTRY_PASSWORD
                                             --export-providers EXPORT_PROVIDERS [EXPORT_PROVIDERS ...]
                                             --import-providers IMPORT_PROVIDERS [IMPORT_PROVIDERS ...]
                                             [--provider-custom-repo-names CUSTOM_REPO_NAMES]
                                             [--provider-custom-branch-names CUSTOM_BRANCH_NAMES]

Deploying Coriolis components on a VM:

    usage: coriolis-cd deploy coriolis components [-h] --host HOST --username
                                              USERNAME --password PASSWORD
                                              --docker-registry REGISTRY_HOST
                                              --docker-registry-user
                                              REGISTRY_USERNAME
                                              --docker-registry-password
                                              REGISTRY_PASSWORD
                                              --docker-pull-images DOCKER_PULL

Deploying an existing appliance from a file url:

    usage: coriolis-cd deploy appliance [-h] [-f {json,shell,table,value,yaml}]
                                    [-c COLUMN] [--noindent] [--prefix PREFIX]
                                    [--max-width <integer>] [--fit-width]
                                    [--print-empty] --provider PROVIDER
                                    --connection-info CONNECTION_INFO
                                    --environment-info ENVIRONMENT_INFO
                                    --vm-identifier VM_NAME --appliance-url
                                    APPLIANCE_URL

Deploying a BM virtual machine used in Coriolis BM tests:

    usage: coriolis-cd baremetal machine deploy [-h] --platform PLATFORM
                                   --connection-info CONNECTION_INFO
                                   --vm-config VM_CONFIGURATION
                                   --appliance-config APPLIANCE_CONFIGURATION

Register BM machine in Coriolis including Snapshot Agent installation:

    usage: coriolis-cd baremetal machine register [-h] --platform PLATFORM
                                   --connection-info CONNECTION_INFO
                                   --vm-config VM_CONFIGURATION
                                   --appliance-config APPLIANCE_CONFIGURATION

Delete BM machine used in Coriolis BM tests:

    usage: coriolis-cd baremetal machine delete --platform PLATFORM
                                   --connection-info CONNECTION_INFO
                                   --vm-config VM_CONFIGURATION

Get BM network name for automated test setup:

    usage: coriolis-cd baremetal machine get network --id BM_MACHINE_ID
                                   --bm-endpoint BM_ENDPOINT
                                   --auth-url CORIOLIS_KEYSTONE_URL
                                   --user-name CORIOLIS_KEYSTONE_USERNAME
                                   --user-password CORIOLIS_KEYSTONE_PASSWORD
                                   --project-name CORIOLIS_PROJECT_NAME
                                   --user-domain-name CORIOLIS_USER_DOMAIN
                                   --project-domain-nam  CORIOLIS_PROJECT_DOMAIN


For more info on any of these commands, simply run:
    coriolis-cd help <command>
